package com.sadesh.discuss

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import com.sadesh.discuss.data.DiscussionDataService
import com.sadesh.discuss.data.DiscussionRepo
import com.sadesh.discuss.data.DiscussionRepoInitializer
import com.sadesh.discuss.data.model.Discussion
import com.sadesh.discuss.ui.create.CreateDiscussionViewModel
import com.sadesh.discuss.ui.create.data.CreateDiscussionDataProvider
import com.sadesh.discuss.ui.list.DiscussionListViewModel
import com.sadesh.discuss.ui.list.data.DiscussionListDataProvider
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DiscussionTest {

    @get:Rule
    val overrideSchedulersRule = RxSchedulersOverrideRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var discussionDataService: DiscussionDataService
    lateinit var discussionListViewModel: DiscussionListViewModel
    lateinit var createDiscussionViewModel: CreateDiscussionViewModel

    @Before
    fun setup() {

        val discussionRepo = DiscussionRepo(DiscussionRepoInitializer().loadExistingDiscussions())
        discussionDataService = DiscussionDataService(discussionRepo)

        discussionListViewModel = DiscussionListViewModel(DiscussionListDataProvider(discussionDataService))
        createDiscussionViewModel = CreateDiscussionViewModel(CreateDiscussionDataProvider(discussionDataService))
    }

    private fun initializeLifeCycle() {
        val lifecycle = LifecycleRegistry(Mockito.mock(LifecycleOwner::class.java))
        lifecycle.addObserver(discussionListViewModel)
        lifecycle.addObserver(createDiscussionViewModel)

        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)

        Thread.sleep(100)
    }

    @Test
    fun testListRetrieval() {

        initializeLifeCycle()

        assertNotNull(discussionListViewModel.viewState.value)

        discussionListViewModel.viewState.value?.let { viewState ->
            assertEquals(20, viewState.discussions.size)
        }

    }

    @Test
    fun testUpVote() {

        initializeLifeCycle()

        assertNotNull(discussionListViewModel.viewState.value)

        var discussions = listOf<Discussion>()

        discussionListViewModel.viewState.value?.let { viewState ->
            discussions = viewState.discussions
        }

        val discussion = discussions[0]
        val currentVote = discussion.upVotes

        discussionListViewModel.vote(discussion.id, true)

        Thread.sleep(100)

        discussionListViewModel.viewState.value?.let { viewState ->
            assertEquals(20, viewState.discussions.size)
            assertEquals(currentVote + 1, viewState.discussions[0].upVotes)
        }

    }

    @Test
    fun testDownVote() {

        initializeLifeCycle()

        assertNotNull(discussionListViewModel.viewState.value)

        var discussions = listOf<Discussion>()

        discussionListViewModel.viewState.value?.let { viewState ->
            discussions = viewState.discussions
        }

        val discussion = discussions[0]
        val currentVote = discussion.downVotes

        discussionListViewModel.vote(discussion.id, false)

        Thread.sleep(100)

        discussionListViewModel.viewState.value?.let { viewState ->
            assertEquals(20, viewState.discussions.size)
            assertEquals(currentVote + 1, viewState.discussions[0].downVotes)
        }

    }

    @Test
    fun testAddDiscussion() {

        initializeLifeCycle()

        createDiscussionViewModel.createDiscussion(subject = "TestSubject")

        Thread.sleep(100)

        assertNotNull(discussionListViewModel.viewState.value)
        assertNotNull(createDiscussionViewModel.viewState.value)

        discussionListViewModel.viewState.value?.let { viewState ->
            assertEquals(21, viewState.discussions.size)
            assertEquals("TestSubject", viewState.discussions[0].subject)
        }

    }

    @Test(expected = IllegalArgumentException::class)
    fun testAddDiscussionWithLongSubject() {

        initializeLifeCycle()

        val subject = "This is a very long subject to test if the DiscussionDataService works fine. " +
                "If all good then we should get the IllegalStateException as planned. " +
                "Don't panic. Unlike other cases Exception is good here. Hopefully, this is long enough to get that." +
                "Ouch! Turned out, it's not. Let me type some more. Yup! got it this time. "

        createDiscussionViewModel.createDiscussion(subject = subject)

        Thread.sleep(100)

    }

}