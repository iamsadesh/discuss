package com.sadesh.discuss.data

import com.sadesh.discuss.data.model.Discussion

/**
 * A simple class that imitates the data persistence.
 *
 * It just holds a list of discussions. The existing discussions are passed during the creation.
 */
class DiscussionRepo(existingDiscussions: List<Discussion> = listOf()) {

    val discussions = mutableListOf<Discussion>()

    init {
        discussions.addAll(existingDiscussions)
    }

}