package com.sadesh.discuss.data

import com.sadesh.discuss.data.model.Discussion
import io.reactivex.Completable
import io.reactivex.subjects.BehaviorSubject

class DiscussionDataService(private val discussionRepo: DiscussionRepo) {

    private val discussionListSubject: BehaviorSubject<List<Discussion>> = BehaviorSubject.create<List<Discussion>>()

    init {
        notifyObservers()
    }

    /**
     * Discussions are provided as Subject so that the Observers can continuously listen
     * to the updates.
     */
    fun discussions(): BehaviorSubject<List<Discussion>> {
        return discussionListSubject
    }

    /**
     * Notifies the latest discussion list to all the listening observers.
     */
    private fun notifyObservers() {
        discussionListSubject.onNext(discussionRepo.discussions.toList())
    }

    /**
     * Adds the discussion into the repo in a synchronized block and notifies
     * the "discussions" subject listeners with the new list.
     *
     * discussion: new discussion to add.
     */
    fun addDiscussion(discussion: Discussion): Completable {

        if (discussion.subject.length > 250) {
            throw IllegalArgumentException("Subject length can't be more than 250 chars")
        }

        return Completable.create { e ->
            doAddDiscussion(discussion)
            e.onComplete()
        }
    }

    private fun doAddDiscussion(discussion: Discussion) {

        synchronized(discussionRepo.discussions, {
            discussionRepo.discussions.add(0, discussion)
            notifyObservers()
        })
    }

    /**
     * Up-votes or down-votes the discussion with the given id in a synchronized block and notifies
     * the "discussions" subject listeners with the updated list.
     *
     * id: the id of the discussion to up-vote
     * upVote: true to up-vote, false to down-vote
     */
    fun vote(id: String, upVote: Boolean): Completable {
        return Completable.create { e ->
            doVote(id, upVote)
            e.onComplete()
        }
    }

    private fun doVote(id: String, upVote: Boolean) {

        synchronized(discussionRepo.discussions, {

            val index = discussionRepo.discussions.indexOfFirst { it.id == id }

            if (index >= 0) {
                val discussion = discussionRepo.discussions[index]
                val upVotes = if (upVote) discussion.upVotes + 1 else discussion.upVotes
                val downVotes = if (upVote) discussion.downVotes else discussion.downVotes + 1

                discussionRepo.discussions[index] = discussion.copy(
                        upVotes = upVotes,
                        downVotes = downVotes
                )
            }

            notifyObservers()
        })

    }

}