package com.sadesh.discuss.data

import com.sadesh.discuss.data.model.Discussion
import java.util.*

/**
 * A helper class to provide initial data for the Repo.
 */
class DiscussionRepoInitializer {

    fun loadExistingDiscussions(): List<Discussion> {

        val discussions = mutableListOf<Discussion>()

        (1..20).forEach { index -> discussions.add(createMockDiscussion(index)) }

        discussions.sortByDescending { it.upVotes - it.downVotes }

        return discussions
    }

    private fun createMockDiscussion(index: Int): Discussion {
        val subject = createMockSubject(index)
        val random = Random()
        return Discussion(
                subject = subject,
                upVotes = random.nextInt(200).toLong(),
                downVotes = random.nextInt(200).toLong()
        )
    }

    private fun createMockSubject(index: Int): String {
        return "$index: This is a mock subject which will be replaced by something else nice."
    }

}