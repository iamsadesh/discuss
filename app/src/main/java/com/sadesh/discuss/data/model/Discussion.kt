package com.sadesh.discuss.data.model

import java.util.*

data class Discussion(val id: String = UUID.randomUUID().toString(),
                      val subject: String,
                      val upVotes: Long = 0,
                      val downVotes: Long = 0,
                      val created: Long = System.currentTimeMillis())