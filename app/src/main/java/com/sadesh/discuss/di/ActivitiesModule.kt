package com.sadesh.discuss.di

import com.sadesh.discuss.ui.create.CreateDiscussionActivity
import com.sadesh.discuss.ui.create.di.CreateDiscussionModule
import com.sadesh.discuss.ui.list.DiscussionListActivity
import com.sadesh.discuss.ui.list.di.DiscussionListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [DiscussionListModule::class])
    abstract fun contributeDiscussionListActivityInjector(): DiscussionListActivity

    @ContributesAndroidInjector(modules = [CreateDiscussionModule::class])
    abstract fun contributeCreateDiscussionActivityInjector(): CreateDiscussionActivity

}