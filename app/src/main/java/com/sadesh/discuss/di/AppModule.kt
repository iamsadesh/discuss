package com.sadesh.discuss.di

import android.content.Context
import com.sadesh.discuss.App
import com.sadesh.discuss.data.DiscussionDataService
import com.sadesh.discuss.data.DiscussionRepo
import com.sadesh.discuss.data.DiscussionRepoInitializer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: App): Context {
        return application.applicationContext
    }

    @Provides
    fun providesDiscussionRepoHelper(): DiscussionRepoInitializer {
        return DiscussionRepoInitializer()
    }

    @Provides
    @Singleton
    fun providesDiscussionRepo(discussionRepoInitializer: DiscussionRepoInitializer): DiscussionRepo {
        return DiscussionRepo(discussionRepoInitializer.loadExistingDiscussions())
    }

    @Provides
    @Singleton
    fun providesDiscussionDataService(discussionRepo: DiscussionRepo): DiscussionDataService {
        return DiscussionDataService(discussionRepo)
    }

}