package com.sadesh.discuss.di

import com.sadesh.discuss.App
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ActivitiesModule::class])
interface AppComponent {

    fun inject(app: App)

}