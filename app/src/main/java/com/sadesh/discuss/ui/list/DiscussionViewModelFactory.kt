package com.sadesh.discuss.ui.list

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.sadesh.discuss.ui.list.data.DiscussionListDataProvider

class DiscussionViewModelFactory(
        private val dataProvider: DiscussionListDataProvider
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            DiscussionListViewModel(dataProvider) as T
}