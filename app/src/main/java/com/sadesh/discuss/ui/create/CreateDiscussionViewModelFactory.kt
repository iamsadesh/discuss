package com.sadesh.discuss.ui.create

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.sadesh.discuss.ui.create.data.CreateDiscussionDataProvider

class CreateDiscussionViewModelFactory(private val dataProvider: CreateDiscussionDataProvider
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            CreateDiscussionViewModel(dataProvider) as T
}