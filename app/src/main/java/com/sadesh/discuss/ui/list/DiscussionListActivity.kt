package com.sadesh.discuss.ui.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.sadesh.discuss.R
import com.sadesh.discuss.data.model.Discussion
import com.sadesh.discuss.ui.create.CreateDiscussionActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_discussion_list.*
import kotlinx.android.synthetic.main.content_activity_list.*
import javax.inject.Inject

class DiscussionListActivity : AppCompatActivity(), Observer<DiscussionListViewModel.ViewState> {

    private lateinit var viewModel: DiscussionListViewModel

    @Inject
    lateinit var viewModelFactory: DiscussionViewModelFactory

    lateinit var adapter: DiscussionListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discussion_list)
        setSupportActionBar(toolbar)

        initViewModel()
        initView()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(DiscussionListViewModel::class.java)
        lifecycle.addObserver(viewModel)

        viewModel.viewState.observe(this, this)
    }

    private fun initView() {

        fab.setOnClickListener {
            val intent = Intent(this, CreateDiscussionActivity::class.java)
            startActivity(intent)
        }

        discussionListView.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false)

        // The following set up is to increase the smoothness of the RecyclerView scrolling.
        discussionListView.setHasFixedSize(true)
        discussionListView.setItemViewCacheSize(20)
        discussionListView.isDrawingCacheEnabled = true

        adapter = DiscussionListAdapter(this)
        discussionListView.adapter = adapter

        adapter.setOnVoteListener(object : OnVoteListener {
            override fun onVote(discussion: Discussion, isUpVote: Boolean) {
                viewModel.vote(discussion.id, isUpVote)
            }
        })

    }

    override fun onChanged(state: DiscussionListViewModel.ViewState?) {
        state?.let {
            renderState(state)
        }
    }

    private fun renderState(viewState: DiscussionListViewModel.ViewState) {
        adapter.setItems(viewState.discussions)
    }
}
