package com.sadesh.discuss.ui.list

import android.arch.lifecycle.*
import com.sadesh.discuss.data.model.Discussion
import com.sadesh.discuss.ui.list.data.DiscussionListDataProvider

/**
 * This ViewModel is an extension of the ViewModel provided by the Architecture components.
 * The benefit here is that it survives the configuration changes and keeps the data intact.
 */
class DiscussionListViewModel(val dataProvider: DiscussionListDataProvider) : ViewModel(), LifecycleObserver {

    /**
     * Data class representing the ViewState. The Controller observes this ViewState and updates
     * its views accordingly.
     */
    data class ViewState(val discussions: List<Discussion> = listOf())

    /**
     * LiveData of the ViewState.
     */
    val viewState = MutableLiveData<ViewState>()

    /**
     * The current state of the ViewState.
     */
    private var currentState = ViewState()

    /**
     * This method will be called when the LifeCycle event ON_CREATE is called.
     * Here, we start observing the discussions.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun init() {

        updateViewState(currentState)

        dataProvider.discussions(onSuccess = { discussionList ->
            updateViewState(currentState.copy(discussions = discussionList))
        })

    }

    fun vote(id: String, upVote: Boolean) {
        dataProvider.vote(id, upVote)
    }

    private fun updateViewState(newState: ViewState) {
        currentState = newState
        viewState.value = currentState
    }

    /**
     * This method will be called when the LifeCycle event ON_DESTROY is called.
     * Here, we dispose all the active Observers of DataProvider.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        dataProvider.dispose()
    }

}