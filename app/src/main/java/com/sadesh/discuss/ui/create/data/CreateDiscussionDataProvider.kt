package com.sadesh.discuss.ui.create.data

import com.sadesh.discuss.data.DiscussionDataService
import com.sadesh.discuss.data.model.Discussion
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CreateDiscussionDataProvider(private val discussionDataService: DiscussionDataService) {

    private val compositeDisposable = CompositeDisposable()

    fun addDiscussion(discussion: Discussion, onSuccess: (() -> Unit)? = null, onError: (() -> Unit)? = null) {
        discussionDataService.addDiscussion(discussion)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { compositeDisposable.add(it) }
                .subscribe({ onSuccess?.invoke() }, { onError?.invoke() })
    }

    fun dispose() {
        compositeDisposable.dispose()
    }

}