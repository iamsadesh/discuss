package com.sadesh.discuss.ui.list

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sadesh.discuss.R
import com.sadesh.discuss.data.model.Discussion
import kotlinx.android.synthetic.main.list_item_discussion.view.*

class DiscussionListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private var onVoteListener: OnVoteListener? = null
    lateinit var discussion: Discussion

    private val onUpVoteClickListener = View.OnClickListener {
        onVoteListener?.onVote(discussion, true)
    }

    private val onDownVoteClickListener = View.OnClickListener {
        onVoteListener?.onVote(discussion, false)
    }

    companion object {
        fun create(context: Context, parent: ViewGroup?): DiscussionListViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.list_item_discussion, parent, false)
            return DiscussionListViewHolder(view)
        }
    }

    fun bind(discussion: Discussion, onVoteListener: OnVoteListener?) {

        this.onVoteListener = onVoteListener
        this.discussion = discussion

        view.subjectTextView.text = discussion.subject

        val votes = discussion.upVotes - discussion.downVotes

        if (votes > 0) {
            view.voteCountTextView.text = votes.toString()
            view.voteCountTextView.setTextColor(ContextCompat.getColor(view.context, R.color.colorPositiveCount))
        } else if (votes < 0) {
            view.voteCountTextView.text = votes.toString()
            view.voteCountTextView.setTextColor(ContextCompat.getColor(view.context, R.color.colorNegativeCount))
        } else {
            view.voteCountTextView.text = ""
        }

        view.upVoteImageView.setOnClickListener(onUpVoteClickListener)

        view.downVoteImageView.setOnClickListener(onDownVoteClickListener)

    }

}