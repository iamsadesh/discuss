package com.sadesh.discuss.ui.list.di

import com.sadesh.discuss.data.DiscussionDataService
import com.sadesh.discuss.ui.list.DiscussionViewModelFactory
import com.sadesh.discuss.ui.list.data.DiscussionListDataProvider
import dagger.Module
import dagger.Provides

@Module
class DiscussionListModule {

    @Provides
    fun providesDiscussionListDataProvider(discussionDataService: DiscussionDataService): DiscussionListDataProvider {
        return DiscussionListDataProvider(discussionDataService)
    }

    @Provides
    fun providesDiscussionListViewModelFactory(dataProvider: DiscussionListDataProvider): DiscussionViewModelFactory {
        return DiscussionViewModelFactory(dataProvider)
    }

}