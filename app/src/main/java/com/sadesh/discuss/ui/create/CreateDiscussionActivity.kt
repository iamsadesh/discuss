package com.sadesh.discuss.ui.create

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.sadesh.discuss.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_create_discussion.*
import javax.inject.Inject

class CreateDiscussionActivity : AppCompatActivity(), Observer<CreateDiscussionViewModel.ViewState> {

    @Inject
    lateinit var viewModelFactory: CreateDiscussionViewModelFactory

    private lateinit var viewModel: CreateDiscussionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_create_discussion)

        initViewModel()

    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CreateDiscussionViewModel::class.java)
        lifecycle.addObserver(viewModel)

        viewModel.viewState.observe(this, this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater?.inflate(R.menu.menu_create_discussion, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (item.itemId) {
                R.id.menu_item_done -> createDiscussion()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createDiscussion() {
        viewModel.createDiscussion(discussionEditText.text)
        discussionEditText.text = ""
    }

    override fun onChanged(state: CreateDiscussionViewModel.ViewState?) {
        state?.let {
            renderState(state)
        }
    }

    private fun renderState(viewState: CreateDiscussionViewModel.ViewState) {
        if (viewState.created) {
            finish()
        }
    }

}