package com.sadesh.discuss.ui.list

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.sadesh.discuss.data.model.Discussion

class DiscussionListAdapter(private val context: Context) : RecyclerView.Adapter<DiscussionListViewHolder>() {

    private val items = mutableListOf<Discussion>()
    private var onVoteListener: OnVoteListener? = null

    fun setItems(newItems: List<Discussion>) {
        val diffResult = DiffUtil.calculateDiff(DiscussionListDiff(this.items, newItems))
        this.items.clear()
        this.items.addAll(newItems)
        diffResult.dispatchUpdatesTo(this)
    }

    fun setOnVoteListener(onVoteListener: OnVoteListener) {
        this.onVoteListener = onVoteListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscussionListViewHolder {
        return DiscussionListViewHolder.create(context, parent)
    }

    override fun getItemCount(): Int = items.size

    private fun getItem(index: Int) = items[index]

    override fun onBindViewHolder(holder: DiscussionListViewHolder, position: Int) {
        holder.bind(getItem(position), onVoteListener)
    }
}

interface OnVoteListener {
    fun onVote(discussion: Discussion, isUpVote: Boolean)
}