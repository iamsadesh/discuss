package com.sadesh.discuss.ui.list

import android.support.v7.util.DiffUtil
import com.sadesh.discuss.data.model.Discussion

class DiscussionListDiff(private val oldItems: List<Discussion>,
                         private val newItems: List<Discussion>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition].id.equals(newItems[newItemPosition].id)
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

}