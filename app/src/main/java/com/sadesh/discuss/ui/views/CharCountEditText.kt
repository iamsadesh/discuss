package com.sadesh.discuss.ui.views

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.sadesh.discuss.R
import kotlinx.android.synthetic.main.view_char_count_edit_text.view.*

/**
 * A standalone View that shows a counter for the remaining typable characters of EditText on top of it.
 * This will be really useful for the usecases where the characters in the EditText are limited.
 */
class CharCountEditText : LinearLayout {

    companion object {
        const val DefaultMaxCharCount = 250
    }

    constructor(context: Context) : super(context) {
        setUpView(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setUpView(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setUpView(attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        setUpView(attrs)
    }

    private var maxCharCount: Int = DefaultMaxCharCount
    private var positiveCountColor: Int = ContextCompat.getColor(context, R.color.colorPositiveCount)
    private var negativeCountColor: Int = ContextCompat.getColor(context, R.color.colorNegativeCount)

    var text: String
        get() = editText.text.toString()
        set(value) {
            editText.setText(value)
        }

    private fun setUpView(attrs: AttributeSet?) {

        orientation = VERTICAL

        LayoutInflater.from(context).inflate(R.layout.view_char_count_edit_text, this)

        attrs?.let {
            val a = context.theme.obtainStyledAttributes(attrs, R.styleable.CharCountEditText, 0, 0)

            try {
                maxCharCount = a.getInt(R.styleable.CharCountEditText_maxChars, DefaultMaxCharCount)
                positiveCountColor = a.getColor(R.styleable.CharCountEditText_countTextColor, positiveCountColor)
                negativeCountColor = a.getColor(R.styleable.CharCountEditText_warningCountTextColor, negativeCountColor)
                editText.hint = a.getString(R.styleable.CharCountEditText_android_hint)
            } catch (e: Exception) {
                a.recycle()
            }
        }

        updateCharCount("")

        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    updateCharCount(s.toString())
                    if (s.length > maxCharCount) {
                        text = s.toString().substring(0, maxCharCount)
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    private fun updateCharCount(text: String) {
        val count = maxCharCount - text.length
        charCountTextView.text = count.toString()

        if (count > 20) {
            charCountTextView.setTextColor(positiveCountColor)
        } else {
            charCountTextView.setTextColor(negativeCountColor)
        }
    }

}
