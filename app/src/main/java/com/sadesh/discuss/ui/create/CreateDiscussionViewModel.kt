package com.sadesh.discuss.ui.create

import android.arch.lifecycle.*
import com.sadesh.discuss.data.model.Discussion
import com.sadesh.discuss.ui.create.data.CreateDiscussionDataProvider

/**
 * This ViewModel is an extension of the ViewModel provided by the Architecture components.
 * The benefit here is that it survives the configuration changes and keeps the data intact.
 */
class CreateDiscussionViewModel(private val dataProvider: CreateDiscussionDataProvider)
    : ViewModel(), LifecycleObserver {

    /**
     * Data class representing the ViewState. The Controller observes this ViewState and updates
     * its views accordingly.
     */
    data class ViewState(val created: Boolean = false)

    /**
     * LiveData of the ViewState.
     */
    val viewState = MutableLiveData<ViewState>()

    /**
     * The current state of the ViewState.
     */
    private var currentState = ViewState()

    /**
     * This method will be called when the LifeCycle event ON_CREATE is called.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun init() {
        updateViewState(currentState)
    }

    fun createDiscussion(subject: String) {

        val discussion = Discussion(subject = subject)

        dataProvider.addDiscussion(discussion, onSuccess = {
            updateViewState(currentState.copy(created = true))
        })

    }

    private fun updateViewState(newState: ViewState) {
        currentState = newState
        viewState.value = newState
    }

    /**
     * This method will be called when the LifeCycle event ON_DESTROY is called.
     * Here, we dispose all the active Observers of DataProvider.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        dataProvider.dispose()
    }

}