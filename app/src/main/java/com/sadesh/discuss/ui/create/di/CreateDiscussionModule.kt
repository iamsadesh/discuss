package com.sadesh.discuss.ui.create.di

import com.sadesh.discuss.data.DiscussionDataService
import com.sadesh.discuss.ui.create.CreateDiscussionViewModelFactory
import com.sadesh.discuss.ui.create.data.CreateDiscussionDataProvider
import dagger.Module
import dagger.Provides

@Module
class CreateDiscussionModule {

    @Provides
    fun providesCreateDiscussionDataProvider(discussionDataService: DiscussionDataService): CreateDiscussionDataProvider {
        return CreateDiscussionDataProvider(discussionDataService)
    }

    @Provides
    fun providesCreateDiscussionViewModelFactory(dataProvider: CreateDiscussionDataProvider): CreateDiscussionViewModelFactory {
        return CreateDiscussionViewModelFactory(dataProvider)
    }

}