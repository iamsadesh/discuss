package com.sadesh.discuss.ui.list.data

import com.sadesh.discuss.data.DiscussionDataService
import com.sadesh.discuss.data.model.Discussion
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class DiscussionListDataProvider(private val discussionDataService: DiscussionDataService) {

    private val compositeDisposable = CompositeDisposable()

    fun discussions(onSuccess: ((List<Discussion>) -> Unit)? = null, onError: ((Throwable) -> Unit)? = null) {
        discussionDataService.discussions()
                .map {
                    it.sortedByDescending { it.upVotes - it.downVotes }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    compositeDisposable.add(it)
                }
                .subscribe({ onSuccess?.invoke(it) }, { onError?.invoke(it) })
    }

    fun vote(id: String, upVote: Boolean, onSuccess: (() -> Unit)? = null, onError: ((Throwable) -> Unit)? = null) {
        discussionDataService.vote(id, upVote)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    compositeDisposable.add(it)
                }
                .subscribe({ onSuccess?.invoke() }, { onError?.invoke(it) })
    }

    fun dispose() {
        compositeDisposable.dispose()
    }

}