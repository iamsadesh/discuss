# Discuss

A simple in-memory clone of Reddit to demonstrate MVVM, Dagger, RxJava and Kotlin usages.

The packages are structured by feature. 

The "data" package contains the model, storage and access classes. 

The "di" package contains the global components and modules needed for dependency injection.

The "ui" package contains the features. There are 2 features as follows and each one is self contained withint its own package.

The "ui.list" encapsulates the Discussion list. It has its own dependency injection classes and data providers to talk to the data storage.

The "ui.create" encapsulates the creation of new discussion. It has its own dependency injection classes and data providers to talk to the data storage.

In both the cases, each feature is represented by an Activity which observes the changes in the ViewState(LiveData) of the ViewModel(Architecture Component) and re-renders its state accordingly. Each ViewModel has its own DataProvider which not only provides the data but also modifies it if asked.

Apart from the above, there is another package inside the "ui" namely "ui.views". This one contains the reusable custom Views that can be used for the whole project. Currently, it holds only one custom View called "CharCountEditText", which shows remaining characters to type in the char-limited EditText.

The Junit test cases are written with Rules to mock the Rx Schedulers and LiveData. Also, Mockito is used for mocking the LifeCycle. 